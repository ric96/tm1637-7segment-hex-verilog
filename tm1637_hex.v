module top(
    input clk,
    inout tm_scl,
    inout tm_sda,
    );

    // setup reset and counter registers
    reg rst = 1;

    // set up tm1637 i2c-like bus
    // yosys has issues doing tristate the usual way
    // so have to define the SB_IO block manually
    reg scl_en;
    reg scl_out;
    SB_IO #(
        .PIN_TYPE(6'b101001),
        .PULLUP(1'b 0)
    ) tm_scl_io (
        .PACKAGE_PIN(tm_scl),
        .OUTPUT_ENABLE(scl_en),
        .D_OUT_0(scl_out)
    );

    reg sda_en;
    reg sda_out;
    reg sda_in;
    SB_IO #(
        .PIN_TYPE(6'b101001),
        .PULLUP(1'b 0)
    ) tm_sda_io (
        .PACKAGE_PIN(tm_sda),
        .OUTPUT_ENABLE(sda_en),
        .D_OUT_0(sda_out),
        .D_IN_0(sda_in)
    );

    // setup led outputs on dev board

    // setup the tm1637 module and registers
    reg hex_latch;
    reg [15:0] hex_data;
    reg hex_busy;

    hex hex_disp (
        clk,
        rst,

        hex_latch,
        hex_data,
        hex_busy,

        scl_en,
        scl_out,
        sda_en,
        sda_out,
        sda_in
    );

    reg [4:0] counter;

    always @(posedge clk) begin
        if (rst) begin
            hex_data <= 0;

            rst <= 0;

        end else if (counter == 0 && !hex_busy) begin
            hex_data <= hex_data + 1;
            hex_latch <= 1;

        end else begin
            hex_latch <= 0;

        end

        counter <= counter + 1;

    end

endmodule


module hex(
    clk,
    rst,
    data_latch,
    data_in,
    busy,
    scl_en,
    scl_out,
    sda_en,
    sda_out,
    sda_in
    );

    input clk;
    input rst;
    input data_latch;
    input [15:0] data_in;
    input sda_in;

    output reg busy;
    output wire scl_en;
    output wire scl_out;
    output wire sda_en;
    output wire sda_out;

    reg [3:0] hex_in;
    wire [6:0] seg_out;

    hex_to_seg hex_disp (
        hex_in,
        seg_out
    );

    // setup the tm1637 module and registers
    reg [7:0] tm_byte;
    reg tm_latch;
    reg tm_stop_bit;
    wire tm_busy;

    tm1637 disp (
        clk,
        rst,

        tm_latch,
        tm_byte,
        tm_stop_bit,
        tm_busy,

        scl_en,
        scl_out,
        sda_en,
        sda_out,
        sda_in
    );

    localparam [3:0]
        S_IDLE          = 4'h0,
        S_DATAMODE      = 4'h1,
        S_LOCATION      = 4'h2,
        S_WRITE_HEX0    = 4'h3,
        S_WRITE_HEX1    = 4'h4,
        S_WRITE_HEX2    = 4'h5,
        S_WRITE_HEX3    = 4'h6,
        S_DISPLAY       = 4'h7,
        S_LATCH         = 4'h8;

    reg [3:0] cur_state;
    reg [3:0] next_state;

    always @(posedge clk) begin
        if (rst) begin
            cur_state <= S_IDLE;
            next_state <= S_IDLE;
            busy <= 0;

        end else if (data_latch) begin
            // if we're already busy, we will just interupt the
            // current display sequence. The current command will
            // be completed first, then the display will start
            // receiving the new data that was freshly latched
            cur_state <= S_DATAMODE;
            busy <= 1;

        end else if (busy && tm_busy == 0) begin
            case (cur_state)
                S_IDLE: begin
                    busy <= 0;
                end

                S_DATAMODE: begin
                    // | 0 | 1 | x | x | N | I | W | 0 |
                    // N = Normal mode (0), I = Address increment (0),
                    // W = Write (0)
                    tm_byte <= 8'b01000000;
                    tm_stop_bit <= 1;
                    tm_latch <= 1;

                    cur_state <= S_LATCH;
                    next_state <= S_LOCATION;
                end

                S_LOCATION: begin
                    // | 1 | 1 | x | x | 0 | 0 | U | L |
                    // U = upper addr, L = lower addr
                    tm_byte <= 8'b11000000;
                    tm_stop_bit <= 0;
                    tm_latch <= 1;

                    // load first digit into hex_to_seg
                    hex_in <= data_in[15:12];

                    cur_state <= S_LATCH;
                    next_state <= S_WRITE_HEX3;
                end

                S_WRITE_HEX3: begin
                    tm_byte <= {1'd0, seg_out};
                    tm_stop_bit <= 0;
                    tm_latch <= 1;

                    // load second digit into hex_to_seg
                    hex_in <= data_in[11:8];

                    cur_state <= S_LATCH;
                    next_state <= S_WRITE_HEX2;
                end

                S_WRITE_HEX2: begin
                    tm_byte <= {1'd0, seg_out};
                    tm_stop_bit <= 0;
                    tm_latch <= 1;

                    // load second digit into hex_to_seg
                    hex_in <= data_in[7:4];

                    cur_state <= S_LATCH;
                    next_state <= S_WRITE_HEX1;
                end

                S_WRITE_HEX1: begin
                    tm_byte <= {1'd0, seg_out};
                    tm_stop_bit <= 0;
                    tm_latch <= 1;

                    // load second digit into hex_to_seg
                    hex_in <= data_in[3:0];

                    cur_state <= S_LATCH;
                    next_state <= S_WRITE_HEX0;
                end

                S_WRITE_HEX0: begin
                    tm_byte <= {1'd0, seg_out};
                    tm_stop_bit <= 1;
                    tm_latch <= 1;

                    cur_state <= S_LATCH;
                    next_state <= S_DISPLAY;
                end

                S_DISPLAY: begin
                    // | 1 | 0 | x | x | D | U | M | L |
                    // D = Display on/off, U = upper brightness bit,
                    // M = mid brightness bit, L = low brightness bit
                    tm_byte <= 8'b10001111;
                    tm_stop_bit <= 1;
                    tm_latch <= 1;

                    cur_state <= S_LATCH;
                    next_state <= S_IDLE;
                end

                S_LATCH: begin
                    tm_latch <= 0;
                    cur_state <= next_state;
                end
            endcase
        end
    end
endmodule


module hex_to_seg(
    input [3:0] hex_digit,
    output [6:0] seg
    );

    reg [6:0] seg;

    always @(hex_digit)
        case (hex_digit)
            4'h0: seg = 7'b0111111;
            4'h1: seg = 7'b0000110;
            4'h2: seg = 7'b1011011;
            4'h3: seg = 7'b1001111;
            4'h4: seg = 7'b1100110;
            4'h5: seg = 7'b1101101;
            4'h6: seg = 7'b1111101;
            4'h7: seg = 7'b0000111;
            4'h8: seg = 7'b1111111;
            4'h9: seg = 7'b1100111;
            4'ha: seg = 7'b1110111;
            4'hb: seg = 7'b1111100;
            4'hc: seg = 7'b0111001;
            4'hd: seg = 7'b1011110;
            4'he: seg = 7'b1111001;
            4'hf: seg = 7'b1110001;
        endcase
endmodule


module tm1637(
    clk,
    rst,
    data_latch,
    data_in,
    data_stop_bit,
    busy,
    scl_en,
    scl_out,
    sda_en,
    sda_out,
    sda_in
    );

    input clk;
    input rst;
    input data_latch;
    input [7:0] data_in;
    input data_stop_bit;
    input sda_in;

    output reg busy;
    output reg scl_en;
    output reg scl_out;
    output reg sda_en;
    output reg sda_out;

    reg [7:0] write_byte;
    reg [2:0] write_bit_count;
    reg write_stop_bit;

    reg [3:0] cur_state;
    reg [3:0] next_state;

    reg [9:0] wait_count;
    localparam [9:0] wait_time = 256; // at 12MHz that's about 47uS

    localparam [3:0]
        S_IDLE      = 4'h0,
        S_WAIT      = 4'h1,
        S_WAIT1     = 4'h2,
        S_START     = 4'h3,
        S_WRITE     = 4'h4,
        S_WRITE1    = 4'h5,
        S_WRITE2    = 4'h6,
        S_WRITE3    = 4'h7,
        S_ACK       = 4'h8,
        S_ACK1      = 4'h9,
        S_ACK2      = 4'hA,
        S_STOP      = 4'hB,
        S_STOP1     = 4'hC,
        S_STOP2     = 4'hD,
        S_STOP3     = 4'hE;

    always @(posedge clk) begin
        if (rst) begin
            // out is set low so enable is used to pull lines low.
            // pull ups on pins will pull high.
            scl_out <= 0;
            sda_out <= 0;

            // output disabled, so lines pulled high
            scl_en <= 0;
            sda_en <= 0;

            // set up FSM
            cur_state <= S_IDLE;
            next_state <= S_IDLE;

            wait_count <= 0;
            write_bit_count <= 0;

            // reset busy flag
            busy <= 0;

        end else begin
            if (data_latch) begin
                // data has been latch
                write_byte <= data_in;
                write_stop_bit <= data_stop_bit;

                // let's rock!
                cur_state <= S_START;
                busy <= 1;

            end else begin
                case (cur_state)
                    S_IDLE: begin
                        // idle waiting for a latch
                        scl_en <= 0;
                        sda_en <= 0;
                        busy <= 0;
                    end

                    S_WAIT: begin
                        // setting up for a wait
                        wait_count <= 0;
                        cur_state <= S_WAIT1;
                    end

                    S_WAIT1: begin
                        // watching the counter till our wait is over
                        wait_count <= wait_count + 1;

                        if (wait_count == wait_time)
                            cur_state <= next_state;
                    end

                    S_START: begin
                        // send the start signal to the bus, then wait
                        sda_en <= 1;

                        cur_state <= S_WAIT;
                        next_state <= S_WRITE;
                    end

                    S_WRITE: begin
                        // tick the clock
                        scl_en <= 1;

                        // reset the bit counts
                        write_bit_count <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_WRITE1;
                    end

                    S_WRITE1:begin
                        // write a bit
                        // 1 to drive bus to low
                        // 0 to HiZ the bus and let pull up do the work
                        sda_en <= ~write_byte[write_bit_count];

                        cur_state <= S_WAIT;
                        next_state <= S_WRITE2;
                    end

                    S_WRITE2: begin
                        // tock the clock
                        scl_en <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_WRITE3;
                    end

                    S_WRITE3: begin
                        if (write_bit_count != 7) begin
                            // advance the bit counter
                            write_bit_count <= write_bit_count + 1;

                            // tick the clock for the next bit
                            scl_en <= 1;
                            cur_state <= S_WRITE1;

                        end else begin
                            // all bits sent, tock the clock
                            scl_en <= 0;

                            cur_state <= S_WAIT;
                            next_state <= S_ACK;
                        end
                    end

                    S_ACK: begin
                        // check the display acknowledged
                        scl_en <= 1;
                        sda_en <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_ACK1;
                    end

                    S_ACK1: begin
                        // tock the clock
                        scl_en <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_ACK2;
                    end

                    S_ACK2: begin
                        // listen for the ack and ack back
                        if (sda_in == 0)
                            sda_en <= 1;

                        cur_state <= S_WAIT;
                        next_state <= write_stop_bit ? S_STOP : S_IDLE;
                    end

                    S_STOP: begin
                        // send stop signal
                        scl_en <= 1;

                        cur_state <= S_WAIT;
                        next_state <= S_STOP1;
                    end

                    S_STOP1: begin
                        sda_en <= 1;

                        cur_state <= S_WAIT;
                        next_state <= S_STOP2;
                    end

                    S_STOP2: begin
                        scl_en <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_STOP3;
                    end

                    S_STOP3: begin
                        sda_en <= 0;

                        cur_state <= S_WAIT;
                        next_state <= S_IDLE;
                    end

                    default: begin
                        cur_state <= S_IDLE;
                    end
                endcase
            end
        end
    end
endmodule
